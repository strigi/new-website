FROM nginx

# dnsutils contains `dig` command for checking dns records
# nano is very useful while debugging config files
RUN apt-get update && apt-get --assume-yes install certbot python-certbot-nginx dnsutils nano;

COPY html /usr/share/nginx/html

# The pem files included are self-signed dummies for eothel.be
# You need to manually invoke certbot from the container to generate real Let's Encrypt certificates using DNS challenge
# e.g. `certbot certonly --manual --preferred-challenges=dns --domains=*.eothel.be,eothel.be`
# Wildcard certificates *.eothel.be are only supported using DNS challenge, and this is not automated yet with my registrar, so it will require manually setting up a TXT record to be able to verify the challenge
# When not using wildcard certificate (i.e. www.eothel.be, foo.eothel.be, ...) it can be automated using a docker-entrypoint.d script.
# The TXT records may take a while to become available, you can check this before continueing in certbot using `dig eothel.be txt`
COPY *.pem /etc/letsencrypt/live/eothel.be/
COPY renew-certificate /root

RUN rm /etc/nginx/conf.d/default.conf
COPY eothel.be.conf /etc/nginx/conf.d
