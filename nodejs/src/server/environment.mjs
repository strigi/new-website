import {logger} from './logger.mjs';

export function isProduction() {
    return process.env.NODE_ENV === 'production';
}

export function param(name, fallback) {
    const value = process.env[name];

    if(isProduction() && !value) {
        throw new Error(`Required environment variable '${name}' was not found. Fallback value '${fallback}' is disallowed because running in production mode.`);
    }

    if(!value) {
        logger.warn(`Required environment variable '${name}' was not found. Using fallback value '${fallback}' because running in development mode. This will throw in production mode!`)
        return fallback;
    }

    logger.debug(`Using required environment variable '${name}' with value '${value}'`);
    return value;
}
