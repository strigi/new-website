import {isProduction} from './environment.mjs';

export class AppError extends Error {
    constructor(message, { cause } = {}) {
        super(message);
        this.name = AppError.name;
        this.id = Math.round(Math.random() * 1_000_000_000).toString(16);
        this.cause = cause;
    }

    toString() {
        return `${this.name}: [${this.id}] ${this.message}`;
    }
}

export class IllegalArgumentError extends AppError {
    static requireType(name, value, type) {
        if(typeof value !== type) {
            throw new IllegalArgumentError(`Argument '${name}' must be of type '${type}' but value was '${value}'`);
        }
    }

    static requireDefined(name, value) {
        if(!value) {
            throw new IllegalArgumentError(`Argument '${name}' is required but was '${value}'`);
        }
    }

    static requireString(name, value) {
        if(typeof value !== 'string') {
            throw new IllegalArgumentError(`Argument '${name}' must be a string but was '${value}'`);
        }
    }

    static requireArray(name, value) {
        if(!Array.isArray(value)) {
            throw new IllegalArgumentError(`Argument '${name}' must be an array but was '${value}'`)
        }
    }

    static requireEnum(name, value, allowedValues) {
        if(!value) {
            throw new IllegalArgumentError(`Argument '${name}' is must be any of '${allowedValues.join(', ')}' but was '${value}'`);
        }
    }

    constructor(message, { cause } = {}) {
        super(message, {cause});
        this.name = AppError.name;
    }
}

export class HttpError extends AppError {
    constructor(status, message, { cause } = {}) {
        super(message, { cause });
        this.name = HttpError.name;
        this.status = status;
        this.headers = {
            'Content-Type': 'application/problem+json',
            'Eothel': 'error'
        }
    }

    toString() {
        return `${this.name}(${this.status}): [${this.id}] ${this.message}`
    }
}

export let errorHandlerMiddleware = (error, req, res, _next) => {
    if(!(error instanceof HttpError)) {
        error = new HttpError(500, 'Internal Server Error', {});
    }

    res.status(error.status);

    Object.keys(error.headers).forEach(headerName => {
        res.set(headerName, error.headers[headerName])
    });

    const responseObject = {
        id: error.id,
        message: error.message,
        details: !isProduction() ? error.stack : undefined,
    };
    res.json(responseObject);
};