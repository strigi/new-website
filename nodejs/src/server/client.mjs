import webpack from 'webpack';
import path from 'path';
import CopyPlugin from 'copy-webpack-plugin';
import express from 'express';
import tailwind from 'tailwindcss';
import { isProduction } from './environment.mjs';
import { logger } from './logger.mjs'

const outputPath = 'build/dist';
const inputPath = 'src/client';

export async function initialize() {
    await build();
    return express.static(outputPath);
}

/**
 * Builds client bundle once for production if `NODE_ENV=production` or in watch mode for development otherwise.
 * @returns {Promise<unknown>}
 */
export function build() {
    return new Promise((resolve, reject) => {
        const compiler = webpack(createWebpackConfig());

        if(isProduction()) {
            logger.info('Building client for production');
            compiler.run(callback);
        } else {
            logger.warn('Starting client compiler in watch mode. Not for production!');
            compiler.watch({}, callback);
        }

        function callback(error, stats) {
            if(error) {
                reject(error);
                return;
            }

            const results = stats.toString({
                colors: true
            });

            if(isProduction() && stats.hasErrors()) {
                reject(new Error(`Unable to build client due to compilation errors: ${results}`));
                return;
            }

            logger.debug(results);

            isProduction() && compiler.close(error => error && reject(error));

            resolve();
        }
    });
}

/**
 * Creates the webpack config typically defined in `webpack.config.js`
 */
function createWebpackConfig() {
    return {
        entry: path.resolve(inputPath, 'main.jsx'),
        output: {
            path: path.resolve(outputPath),
            filename: 'bundle.js',
        },

        mode: determineMode(),
        devtool: 'source-map',

        /**
         * Extensions to resolve allows the imports to look like this: `import { App } from './app.component';` instead of import { App } from './app.component.jsx';
         */
        resolve: {
            extensions: ['.js', '.jsx'],
        },

        module: {
            rules: [{
                test: /\.jsx$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-react']
                    }
                },
            }, {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: createPostCssConfig()
                        }
                    }
                ],
            }],
        },

        plugins: [
            new CopyPlugin({
                patterns: [{
                    from: '*.{html,ico,png}',
                    context: inputPath
                }],
            }),
        ],
    };
}

/**
 * Creates the PostCSS configuration object, typically defined in `postcss.config.js`
 */
function createPostCssConfig() {
    return {
        plugins: [
            'postcss-import',
            tailwind({
                config: createTailwindConfig()
            }),
            'autoprefixer'
        ]
    };
}

/**
 * Creates the Tailwind CSS configuration object, typically defined in `tailwind.config.js`
 */
function createTailwindConfig() {
    return {
        mode: 'jit',
        purge: [
            inputPath + '/**/*.{jsx,html}',
        ],
    };
}

function determineMode() {
    return isProduction() ? 'production' : 'development';
}
