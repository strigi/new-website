import {Passport} from 'passport';
import {Strategy as LocalStrategy} from 'passport-local';
import express from 'express';
import session from 'express-session';
import {AppError, HttpError} from './error.mjs';
import {User} from './user.model.mjs';
import {logger} from './logger.mjs';

const passport = new Passport();

passport.use(new LocalStrategy(function(username, password, done) {
    User.findByUsername(username).then(user => {
        if(!user) {
            logger.info(`Login failed: User '${username}' not found`);
            done(null, false);
            return;
        }

        if(password !== user.password) {
            logger.info(`Login failed: Invalid password for user '${username}'`);
            done(null, false);
            return;
        }

        logger.info(`Login successful: User '${username}'`);
        done(null, user);
    }).catch(done);
}));

passport.serializeUser((user, done) => {
    done(null, JSON.stringify(user));
});

passport.deserializeUser((string, done) => {
    done(null, JSON.parse(string));
});

const passportMiddleware = passport.initialize();

const sessionMiddleware = session({
    secret: 'auth',
    resave: false,
    saveUninitialized: false,
    cookie: {
        sameSite: 'strict',
    }
});

/**
 * Returns a middleware chain for authentication using the specified passport strategy.
 * @param strategy The authentication strategy to use.
 * @returns {Router} Express router for use in an endpoint pre-handler position. `app.get('/foo', auth('session'), (req, res, next) => { ... })`
 */
export function authenticate(strategy) {
    if(strategy !== 'local' && strategy !== 'session') {
        throw new Error('Invalid parameter');
    }

    const router = express.Router();
    if(strategy === 'local') {
        router.use(express.json());
    }
    router.use(sessionMiddleware);
    router.use(passportMiddleware);
    router.use(passport.authenticate(strategy));
    return router;
}

/**
 * Returns a middleware chain that is setup for authorizing the currently logged in user with the session strategy
 * If there is no currently logged on user, throws a 401 error.
 * @returns {Router} Express router for use in an endpoint pre-handler position. `app.get('/foo', disallowAnonymous(), (req, res, next) => { ... })`
 */
export function authorize(anyRequiredPrivilege = []) {
    if(!Array.isArray(anyRequiredPrivilege)) {
        throw new AppError('Invalid parameter');
    }

    const router = authenticate('session');
    router.use((req, res, next) => {
        if(!req.user) {
            throw new HttpError(401, 'Unauthenticated');
        }

        if(anyRequiredPrivilege.length > 0 && !req.user.privileges.some(p => anyRequiredPrivilege.includes(p))) {
            throw new HttpError(403, 'Forbidden');
        }

        next();
    });
    return router;
}
