import chalk from 'chalk';

const levels = {
    debug: chalk.blueBright,
    info: chalk.greenBright,
    warn: chalk.yellowBright,
    error: chalk.redBright
}

export const logger = Object.keys(levels).reduce((logger, level) => {
    logger[level] = write.bind(logger, level);
    return logger;
}, {});

function write(level, message) {
    console[level](`${(new Date().toISOString())} [ ${levels[level](level)} ] ${message}`);
}
