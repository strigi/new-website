import express from 'express';
import {authenticate, authorize} from './auth.mjs';
import * as client from './client.mjs';
import {errorHandlerMiddleware} from './error.mjs';
import mongoose from 'mongoose';
import {param} from './environment.mjs';
import {logger} from './logger.mjs';

try {
    const app = await createExpressApp();
    await connectToMongodb();

    const port = 3000;
    app.listen(port, () => {
        logger.info(`Listening on port ${port}`);
    });
} catch(error) {
    logger.error(`Error while starting up application due to ${error.stack}`);
}

async function createExpressApp() {
    const app = express();

    app.use(await client.initialize());

    app.post('/login', authenticate('local'), (req, res) => {
        res.json(req.user);
    });

    app.get('/api/users/me', authorize(), (req, res) => {
        res.json(req.user);
    });

    app.use(errorHandlerMiddleware);
    return app;
}

async function connectToMongodb() {
    const uri = param('MONGODB_URI', 'mongodb://localhost/eothel');
    logger.info(`Connecting to mongodb '${uri}'`);
    await mongoose.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
}
