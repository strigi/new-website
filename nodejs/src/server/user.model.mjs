import mongoose from 'mongoose';
import {IllegalArgumentError} from './error.mjs';

/**
 * Mongoose Document representing a user account that can login on the website.
 * @typedef {Document} UserDocument
 * @property {string} username The username with which to login.
 * @property {string} password The password with which to login.
 * @property {string} firstName The first name of the user.
 * @property {string} lastName The last name of the user.
 */
const schema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true }
});

/**
 * Retrieves a single user by username
 * @param username {string} The username to query the user by.
 * @returns {Promise<UserDocument>}
 */
schema.statics.findByUsername = function(username) {
    IllegalArgumentError.requireString('username', username);
    return this.findOne({ username });
}

/**
 * Mongoose Model for a user account that can login on the website.
 * @typedef {Model<UserDocument>} UserModel
 * @property {function(username: string): Promise<UserDocument>} findByUsername
 */

/**
 * @type {UserModel}
 * @constructor
 */
export const User = mongoose.model('user', schema);
