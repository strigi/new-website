import React, { createRef } from 'react';

export function Popup(props) {
    const veil = createRef();

    return (
        <div ref={veil} className="fixed flex justify-center items-center w-full h-full top-0 left-0 bg-black/75" onClick={e => handleClose(e)}>
            <div className="rounded shadow p-5 bg-white">
                {props.children}
            </div>
        </div>
    );

    function handleClose(e) {
        if(e.target === veil.current && props.onClose) {
            props.onClose();
        }
    }
}
