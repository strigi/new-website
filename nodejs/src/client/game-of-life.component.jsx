import React from 'react';

React.Component

export function GameOfLife(props) {
    const width = Number(props.width);
    const height = Number(props.height);
    const cellSize = Number(props['cell-size']);

    const [generation, setGeneration] = React.useState(0);
    const [fps, setFps] = React.useState(Number(props.fps) || 5);

    const cellsRef = React.createRef();

    React.useEffect(() => {
        const cells = cellsRef.current.children;

        let timerId = null;

        // Setup
        forEach(reset);
        flip();
        forEach(randomize);
        flip();

        // Start animation loop
        tick();

        // Stop animation loop when component is unmounted
        return stopTick;

        function stopTick() {
            console.info('Stop Game Of Life');
            clearTimeout(timerId);
        }

        function tick() {
            const start = Date.now();
            if(timerId === null) {
                console.info('Start Game Of Life');
            }
            if(fps > 0) {
                timerId = setTimeout(tick, 1000 / fps);
            }
            forEach((x, y) => {
                const c = count(x, y);
                if(get(x, y) > 0) {
                    if(c >= 2 && c <= 3) {
                        inc(x, y);
                    } else {
                        reset(x, y);
                    }
                } else {
                    if(c === 3) {
                        set(x, y, 1);
                    } else {
                        reset(x, y);
                    }
                }
            });
            flip();
            const end = Date.now();
            setGeneration(generation + 1);
            console.debug(`Frametime: ${end - start}ms`);
        }

        function count(x, y) {
            return Math.min(get(x - 1, y - 1), 1) // top left
                + Math.min(get(x + 0, y - 1) , 1) // top center
                + Math.min(get(x + 1, y - 1) , 1) // top right
                + Math.min(get(x - 1, y - 0) , 1) // center left
                                                        // center center (does not count)
                + Math.min(get(x + 1, y - 0) , 1) // center right
                + Math.min(get(x - 1, y + 1) , 1) // bottom left
                + Math.min(get(x + 0, y + 1) , 1) // bottom center
                + Math.min(get(x + 1, y + 1) , 1) // bottom right
        }

        function set(x, y, value) {
            if(x < 0 || x >= width || y < 0 || y >= height) {
                throw new Error(`Cell ${x} ${y} is invalid`);
            }
            cells[y * width + x].next = value;
        }

        function randomize(x, y) {
            set(x, y, Math.random() <= 0.5 ? 1 : 0);
        }

        function inc(x, y) {
            set(x, y, get(x, y) + 1);
        }

        function reset(x, y) {
            set(x, y, 0);
        }

        function get(x, y) {
            if(x < 0 || x >= width || y < 0 || y >= height) {
                return 0;
            }
            return cells[y * width + x].current;
        }

        function flip() {
            forEach((x, y, i) => {
                cells[i].current = cells[i].next;
                const current = get(x, y);
                const colorComponent = Math.min(current * 64, 255);
                const color = `rgb(${colorComponent}, ${colorComponent}, ${colorComponent})`;
                cells[i].querySelector('rect').setAttribute('fill', `${color}`);
                reset(x, y);
            });
        }

        function forEach(fn) {
            let index = 0;
            for(let y = 0; y < height; y++) {
                for(let x = 0; x < width; x++) {
                    fn(x, y, index++);
                }
            }
        }
    });

    return (
        <div>
            <svg width={width * cellSize} height={height * cellSize}>
                <g ref={cellsRef}>{createCells()}</g>
                <g>{createGrid()}</g>
            </svg>
            FPS: <input type="number" min="0" value={fps} onChange={e => setFps(e.target.value )}/>
            {fps === 0 && <output>PAUSED</output>}
            <div>Generation: {generation}</div>
        </div>
    );

    function handleCellClick(e) {
        const cellGroup = e.target.parentNode;
        console.log(cellGroup);
    }

    function key(x, y) {
        return `(${x}, ${y})`;
    }

    function createCells() {
        const cells = [];
        for (let y = 0; y < height; y++) {
            for (let x = 0; x < width; x++) {
                const cell = (
                    <g key={key(x, y)} id={cells.length}>
                        <rect x={x * cellSize} y={y * cellSize} width={cellSize} height={cellSize} fill="black" onClick={e => handleCellClick(e)}/>;
                        {/*<text x={x * cellSize + (cellSize / 2)} y={y * cellSize + (cellSize / 2)} fill="red" textAnchor="middle" dominantBaseline="middle" fontSize="0.75rem">({x}, {y})</text>*/}
                    </g>
                )
                cells.push(cell);
            }
        }
        return cells;
    }

    function createGrid() {
        const lines = [];
        const lineWidth = 1;
        for (let x = 0; x <= width; x++) {
            lines.push(<line key={`x-${x}`} x1={x * cellSize} y1="0" x2={x * cellSize} y2={height * cellSize} stroke="gray" strokeWidth={lineWidth}/>);
        }
        for (let y = 0; y <= height; y++) {
            lines.push(<line key={`y-${y}`} x1="0" y1={y * cellSize} x2={width * cellSize} y2={y * cellSize} stroke="gray" strokeWidth={lineWidth}/>);
        }
        return lines;
    }
}
