import React, {useState} from 'react';
import './style.css';
import {userService} from './user.service';
import {Popup} from './popup.component';
import {Button} from "./button.component";
import {Info} from "./info.component";
import {GameOfLife} from "./game-of-life.component";

export function App() {
    const [showLoginDialog, setShowLoginDialog] = useState(false);
    const [showUnderConstructionDialog, setShowUnderConstructionDialog] = useState(false);
    const [showGameOfLifeDialog, setShowGameOfLifeDialog] = useState(false);
    const [loginInfo, setLoginInfo] = useState(null);

    return <div className="font-sans">
        <nav className="flex">
            <a className="p-5 cursor-pointer" onClick={e => handleUnderConstruction(e) }>Projects</a>
            <a className="p-5 cursor-pointer" onClick={e => handleUnderConstruction(e) }>Electronics</a>
            <a className="p-5 cursor-pointer" onClick={e => setShowGameOfLifeDialog(true)}>Game Of Life</a>
            <div className="flex-grow"/>
            <div className="p-5 font-bold">{userService.user ? `${userService.user.firstName} ${userService.user.lastName}` : 'Anonymous'}</div>
        </nav>
        <div className="flex text-white bg-gray-800 p-5 items-center justify-center">
            <div>
                <h1 className="text-4xl min-w-max">🦉Eothel.be</h1>
                <div>Free software & JavaScript & Electronics</div>
                <div className="mt-5">
                    <Button label="Login" onClick={handleLogin} className="w-32"/>
                </div>
            </div>
            <img src="backdrop.png" className="h-40 md:h-44 sm:ml-10 md:ml-20 hidden sm:block"/>
        </div>

        {/* The login dialog shown when the login button is clicked */}
        {showLoginDialog &&
            <Popup title="Login" onClose={e => handleCancelLogin(e)}>
                <h1 className="text-xl border-b">Login</h1>
                <form className="flex flex-col mt-5 w-72" onSubmit={e => handleLoginSubmit(e)}>
                    <div>
                        <div>Username:</div>
                        <input name="username" className="border rounded p-2 w-full shadow"/>
                    </div>
                    <div className="mt-2">
                        <div>Password:</div>
                        <input name="password" type="password" className="border rounded p-2 w-full shadow"/>
                    </div>
                    <div className="mt-2">
                        {loginInfo && (
                            <Info mode={loginInfo.mode} message={loginInfo.message}/>
                        )}
                    </div>
                    <div className="mt-2 flex">
                        <Button label="Login" className="w-full"/>
                        <Button type="button" label="Cancel" mode="danger" className="ml-3 w-full" onClick={e => handleCancelLogin(e)}/>
                    </div>
                </form>
            </Popup>
        }

        {showUnderConstructionDialog &&
            <Popup onClose={e => handleCloseUnderConstruction(e)}>
                <h1 className="text-xl">🚧 Under Construction 🚧</h1>
            </Popup>
        }

        {showGameOfLifeDialog &&
            <Popup onClose={e => setShowGameOfLifeDialog(false)}>
                <GameOfLife width="75" height="75" cell-size="8" fps="10"/>
            </Popup>
        }
    </div>;

    function handleLogin() {
        setLoginInfo(null);
        setShowLoginDialog(true);
    }

    function handleUnderConstruction() {
        setShowUnderConstructionDialog(true);
    }

    function handleCloseUnderConstruction() {
        setShowUnderConstructionDialog(false);
    }

    async function handleLoginSubmit(e) {
        e.preventDefault();
        const username = e.target.username.value;
        const password = e.target.password.value;
        const authenticationErrorMessage = {mode: 'danger', message: 'Authentication rejected'};
        try {
            const result = await userService.authenticate(username, password);
            if (result === false) {
                setLoginInfo(authenticationErrorMessage);
                return;
            }
            setShowLoginDialog(false);
        } catch(error) {
            setLoginInfo(authenticationErrorMessage);
        }
    }

    function handleCancelLogin(e) {
        setShowLoginDialog(false);
    }
}
