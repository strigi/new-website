import React from 'react';

export function Info(props) {
    const className = 'rounded bg-red-500 text-white p-1';

    return (
        <div className={className}>{props.mode === 'danger' ? '🙁️' : 'ℹ️'}<span className="ml-2">{props.message}</span></div>
    );
}