export const userService = new class {
    #user = null;

    async initialize() {
        console.log('Initializing userService');
        const response = await fetch('/api/users/me');
        if (response.status === 401) {
            this.#user = null;
            return;
        }

        if(response.status === 200) {
            this.#user = await response.json();
            return;
        }

        const body = await response.text();
        throw new Error(`Unable to initialize user service due, status ${response.status} body: ${body}`);
    }

    async authenticate(username, password) {
        console.info(`Authenticating user '${username}'`)
        const response = await fetch('/login', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username, password })
        });

        if(response.status === 401) {
            this.#user = null;
            return false;
        }

        if(response.status === 200) {
            this.#user = await response.json();
            return true;
        }

        const body = await response.text();
        throw new Error(`Error while attempting to authenticate, status: '${response.status}' body: '${body}'`);
    }

    get user() {
        return this.#user;
    }
}
