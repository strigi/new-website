import React from 'react';

export function Button(props) {
    let className = props.className + ' font-bold p-2 rounded text-white shadow';

    if(props.mode === 'danger') {
        className += ' bg-red-500 hover:bg-red-400';
    } else {
        className += ' bg-green-500 hover:bg-green-400';
    }

    return (
        <button type={props.type} className={className} onClick={props.onClick}>{props.label}</button>
    );
}

