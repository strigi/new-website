import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app.component';
import { userService } from './user.service';

(async () => {
    await userService.initialize();
    ReactDOM.render(<App/>, document.querySelector('main'));
})();
